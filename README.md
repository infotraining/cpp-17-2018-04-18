# README #

### Docs ###

* https://infotraining.bitbucket.io/cpp-17/

### Test before training ###

* https://goo.gl/forms/2ruLVURV1TGD9Ytt1

### Proxy settings ###

* add to .profile

```
export http_proxy=http://10.158.100.2:8080
export https_proxy=https://10.158.100.2:8080
```

### Ankieta ###

* https://docs.google.com/forms/d/e/1FAIpQLSfV0RIHHqDvbQjBfrsvn5jq54Qe7l5PL3ioZafxtbQlBCAl6Q/viewform?hl=pl

### Test after training ###

* https://goo.gl/forms/MFkr6graS4fxmdtP2