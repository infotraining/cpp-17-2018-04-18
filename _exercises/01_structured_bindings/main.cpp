#include <iostream>
#include <string>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

class Customer
{
private:
    std::string first_;
    std::string last_;
    u_int8_t age_;

public:
    Customer(std::string f, std::string l, long v)
        : first_(std::move(f))
        , last_(std::move(l))
        , age_(v)
    {
    }

    std::string first() const
    {
        return first_;
    }
    std::string last() const
    {
        return last_;
    }
    long age() const
    {
        return age_;
    }
};

template <>
struct std::tuple_size<Customer>
{
    static const unsigned int value = 3;
};

template <>
struct std::tuple_element<0, Customer>
{
    using type = string;
};

template <>
struct std::tuple_element<1, Customer>
{
    using type = string;
};

template <>
struct std::tuple_element<2, Customer>
{
    using type = long;
};

template <size_t N>
auto get(const Customer& c)
{
    if constexpr(N == 0)
        return c.first();
    else if constexpr(N == 1)
        return c.last();
    else
        return c.age();
}

// template <>
// auto get<0>(const Customer& c)
// {
//     return c.first();
// }

// template <>
// auto get<1>(const Customer& c)
// {
//     return c.last();
// }

// template <>
// auto get<2>(const Customer& c)
// {
//     return c.age();
// }

TEST_CASE("structured bindings with tuple protocol")
{
    Customer c("Jan", "Kowalski", 42);

    auto [f, l, a] = c;

    REQUIRE(f == "Jan");
    REQUIRE(l == "Kowalski");
    REQUIRE(a == 42);
}