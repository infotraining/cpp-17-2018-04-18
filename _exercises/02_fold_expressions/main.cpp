#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <memory>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"


using namespace std;

template <typename Container, typename... T>
size_t matches(const Container& c, T... value)
{
    return (... + count(begin(c), end(c), value));
}

TEST_CASE("matches - returns how many items is stored in a container")
{
    vector<int> v{1, 2, 3, 4, 5};

    REQUIRE(matches(v, 2, 5) == 2);
    REQUIRE(matches(v, 100, 200) == 0);
    REQUIRE(matches("abcdef", 'x', 'y', 'z') == 0);
    REQUIRE(matches("abcdef", 'a', 'd', 'f') == 3);
}

template <typename Container, typename... T>
void push_all(Container& container, T&&... value)
{
    (container.push_back(std::forward<T>(value)), ...);
}

TEST_CASE("push_all - multiple push_backs")
{
    using namespace Catch::Matchers;

    std::vector<int> v {1, 2, 3};
    push_all(v, 4, 5, 6);

    REQUIRE_THAT(v, Equals(vector {1, 2, 3, 4, 5, 6}));

    vector<unique_ptr<int>> ptrs;
    push_all(ptrs, make_unique<int>(5), make_unique<int>(6));

    REQUIRE(ptrs.size() == 2);
}

template <typename Set, typename... T>
bool insert_all(Set& container, T&&... value)
{
    return (... && container.insert(std::forward<T>(value)).second);
}

TEST_CASE("insert_all - performs inserts and returns true if all have been succesful")
{
    set<int> my_set = { 1, 2, 3 };

    REQUIRE(insert_all(my_set, 4, 5, 6) == true);
    REQUIRE(insert_all(my_set, 6, 3) == false);
}

template <typename T>
struct Range : pair<T, T>
{
    Range(T first, T second) : pair<T, T>{first, second}
    {}
};

template <typename T1, typename T2>
Range(T1, T2) -> Range<common_type_t<T1, T2>>;

template <typename T, typename... Ts>
bool within(const Range<T>& range, const Ts&... value)
{
    auto is_within = [&range](const auto& value) { return value >= range.first && value <= range.second; };

    return (... && is_within(value));
}

TEST_CASE("within - checks if all values fit in range [low, high]")
{        
    REQUIRE(within(Range{10,  20.0},  1, 15, 30) == false);
    REQUIRE(within(Range{10,  20},  11, 12, 13) == true);
    REQUIRE(within(Range{5.0, 5.5},  5.1, 5.2, 5.3) == true);
}

template <typename T>
void hash_combine(size_t& seed, const T& value)
{
    seed ^= hash<T>{}(value) + 0x9e3779b9 + (seed<<6) + (seed>>2);
}

template <typename... Ts>
size_t combined_hash(const Ts&... value)
{
    size_t seed = 0;
    (..., hash_combine(seed, value));

    return seed;
}

TEST_CASE("combined_hash - write a function that calculates combined hash value for a given number of arguments")
{
    size_t seed{};

    REQUIRE(combined_hash(1U) == 2654435770U);
    REQUIRE(combined_hash(1, 3.14, "string"s) == 10365827363824479057U);
    REQUIRE(combined_hash(123L, "abc"sv, 234, 3.14f) == 162170636579575197U);
}

////////////////////////////////////////////////////////////////

template <typename... Bases>
struct Derived : Bases...
{
    void print()
    {
        (..., Bases::print());
    }
};

struct A
{
    void print()
    {
        cout << "A";
    }
};

struct B
{
    void print()
    {
        cout << "B";
    }
};

TEST_CASE("multibase with print")
{
    using Multibase = Derived<A, B>;

    Multibase mbs;
    mbs.print();

    cout << endl;
}
