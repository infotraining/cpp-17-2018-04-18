#include <boost/algorithm/string.hpp>
#include <boost/tokenizer.hpp>
#include <fstream>
#include <iostream>
#include <iterator>
#include <map>
#include <unordered_map>
#include <set>
#include <string>
#include <algorithm>

using namespace std;

/*
    Napisz program zliczający ilosc wystapien danego slowa w pliku tekstowym. Wyswietl wyniki zaczynajac od najczesciej wystepujacych slow.
*/

template <class InputIt, class Size, class UnaryFunction>
InputIt for_each_n(InputIt first, Size n, UnaryFunction f)
{
    for (Size i = 0; i < n; ++first, (void)++i)
    {
        f(*first);
    }
    return first;
}

int main()
{
    const string file_name = "proust.txt";

    ifstream fin(file_name);

    if (!fin)
        throw runtime_error("File "s + file_name + " can't be opened");

    unordered_map<string, size_t> word_counter;

    string word;
    while (fin >> word)
    {
        boost::to_lower(word);
        boost::trim_if(word, boost::is_any_of("\"'\t?!,.;:()-/\\"));
        word_counter[move(word)]++;
    }

    multimap<size_t, string_view, greater<int>> concordance;

    for (const auto& [ word, count ] : word_counter)
        //concordance.insert(pair(count, string_view(word)));
        concordance.emplace(count, word);

    cout << "Top 20 words:\n";
    for_each_n(concordance.begin(), 20, [](const auto& kv) { cout << kv.second << " - " << kv.first << endl; });
}
