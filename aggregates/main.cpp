#include <iostream>
#include <string>
#include <complex>
#include <array>
#include <type_traits>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

struct CData
{
    int a;
    double b;
    string text;
    int tab[3];
};

struct Derived : CData
{
    int c;
};

TEST_CASE("aggregates")
{
    CData d1{ 10, 3.14, "text"s, {1, 2, 3}};

    REQUIRE(d1.a == 10);
    REQUIRE(d1.text == "text"s);
    REQUIRE(d1.tab[0] == 1);

    SECTION("can inherit from a class")
    {
        Derived agg = { { 1, 3.14, "abc"s, {} }, 6};

        REQUIRE(agg.c == 6);
        REQUIRE(agg.text == "abc"s);
    }
}

template <typename T>
struct Aggregate : std::string, std::complex<T>
{
    string d;
};

TEST_CASE("complex aggregate")
{
    Aggregate<double> agg{ {"text"}, {4.5, 5.1}, "abc" };
    REQUIRE(agg.d == "abc"s);

    Aggregate<double> agg2{};

    REQUIRE(agg2.d == ""s);
    REQUIRE(agg2.real() == 0);
    REQUIRE(agg2.imag() == 0);

    static_assert(is_aggregate_v<Aggregate<double>>, "Not an aggregate");
}

struct WTF
{
    WTF() = delete;
};

TEST_CASE()
{
    // WTF wtf; // ERROR
    WTF wtf{};
    static_assert(is_aggregate_v<WTF>);
}