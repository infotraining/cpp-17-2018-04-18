#include <iostream>
#include <string>
#include <any>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

TEST_CASE("any")
{
    SECTION("can hold any copyable type")
    {
        any obj;

        REQUIRE_FALSE(obj.has_value());

        obj = 42;
        obj = 3.14;
        obj = "text"s;

        SECTION("can be safely casted to given type")
        {
            auto text = any_cast<string>(obj); // copy of string holded by obj

            REQUIRE(text == "text"s);

            REQUIRE_THROWS_AS(any_cast<double>(obj), bad_any_cast);

            SECTION("better performance with pointer")
            {
                auto ptr_text = any_cast<string>(&obj);

                if (ptr_text)
                {
                    REQUIRE(*ptr_text == "text"s);
                }
            }
        }

        SECTION("type returns type_info")
        {
            REQUIRE(obj.type() == typeid(string));
        }

        SECTION("reset")
        {
            obj.reset();

            REQUIRE(obj.has_value() == false);
        }
    }
}
