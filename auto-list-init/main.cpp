#include <iostream>
#include <string>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

TEST_CASE("Before C++17")
{
    auto x1 = 42; // int
    auto x2(42); // int    
    auto x3{42}; // initializer_list<int>
    static_assert(is_same<decltype(x3), initializer_list<int>>::value, "ERROR");
    auto x4 = {42}; // initializer_list<int>
    //auto x5{34, 23}; // initializer_list<int>
    auto x6 = {45, 24}; // initializer_list<int>
}

TEST_CASE("Since C++17")
{
    auto x1 = 42; // int
    auto x2(42); // int    
    auto x3{42}; // int!!!
    static_assert(is_same<decltype(x3), int>::value, "ERROR");
    auto x4 = {42}; // initializer_list<int>
    //auto x5{34, 23}; // ERROR
    auto x6 = {45, 24}; // initializer_list<int>
}