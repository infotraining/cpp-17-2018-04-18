#include <iostream>
#include <string>
#include <type_traits>
#include <functional>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

// template <typename T>
// void auto_mechanism(T item)
// {    
// }

template <typename T>
void auto_mechanism(T& item)
{
    using VarType = remove_const_t<T>;
    VarType obj;
}

template <typename T>
void auto_mechanism(T&& item)
{    
}

TEST_CASE("auto - description")
{    
    vector<int> vec = {1, 2, 3};

    const int x = 10;
    const int& ref_x = x;
    int tab[10] = {};

    auto_mechanism(ref_x);

    SECTION("1st - case")
    {
        auto x = 42;
        auto_mechanism(42);

        auto new_x1 = x; // int
        auto new_x2 = ref_x; // int
        auto item = vec.at(1); // int
        
        auto tab2 = tab; // int*
        auto_mechanism(tab); // T deduces as int*
    }

    SECTION("2nd - case")
    {
        auto& new_x1 = x; // const int&
        auto& new_x2 = ref_x; // const int&

        auto& tab2 = tab; // int(&)[10]

        auto& item = vec.at(1); // int&
    }

    SECTION("3rd case")
    {
        auto&& rr1 = x; // const int&
        auto&& rr2 = 42; // int&&

        auto&& item = vec.at(1); // int&
        auto&& it = vec.begin(); // vector<int>::iterator&&
    }

    SECTION("lamda with auto")
    {
        [[ maybe_unused ]] auto l = [](const auto& item) {
            using VarType = decay_t<decltype(item)>;
            VarType temp = item;
            //...
        };
    }
}

template <typename T1, typename T2>
class Data
{
public:
    T1 val1;
    T2 val2;
    
    Data(T1 v1, T2 v2) : val1{v1}, val2{v2}
    {}
};

template <typename GuessType, typename Type>
void assert_deduction(Type obj)
{
    static_assert(is_same_v<GuessType, decltype(obj)>);
}

TEST_CASE("class template argument deduction")
{
    Data<int, double> d1{1, 3.14};
    
    Data d2{2, 6.28};
    assert_deduction<Data<int, double>>(d2);

    Data d3{3.14f, "text"s};
    assert_deduction<Data<float, string>>(d3);

    int tab[10] {};
    Data d4{tab, "text"};
    assert_deduction<Data<int*, const char*>>(d4);

    const vector<int> vec = { 1, 2, 3 };
    Data d5(vec.begin(), vec.end());
    assert_deduction<Data<vector<int>::const_iterator, vector<int>::const_iterator>>(d5);

    Data d6{vec.at(1), vec.front()};
    assert_deduction<Data<int, int>>(d6);
}

template <typename T>
struct Data2
{
    T value;

    Data2(const T& v) : value{v}
    {}
};

// explicit deduction guide
template <typename T>
Data2(T) -> Data2<T>;

Data2(const char*) -> Data2<string>;

TEST_CASE("constructor with ref param")
{
    int x = 10;
    Data2 d1{x};
    assert_deduction<Data2<int>>(d1);

    const int& cref = x;
    Data2 d2{cref};
    assert_deduction<Data2<int>>(d2);

    int tab[10] = {};
    Data2 d3{tab};
    assert_deduction<Data2<int*>>(d3);

    Data2 d4{"text"};
    assert_deduction<Data2<string>>(d4);
}

template <typename T>
struct Complex
{
    T real, img;

    Complex(const T& img, const T& real) : real{real}, img{img}
    {}    
};

template <typename T1, typename T2>
Complex(T1, T2) -> Complex<std::common_type_t<T1, T2>>;

TEST_CASE("Complex")
{
    Complex c1{4, 6};

    Complex c2{4.4f, 5.6}; // Complex<double>
}

TEST_CASE("deduction guides for std library")
{
    SECTION("pair")
    {
        pair p1{1, 3.14};
        assert_deduction<pair<int, double>>(p1);

        int tab[10];
        pair p2{tab, "text"};
        assert_deduction<pair<int*, const char*>>(p2);
    }

    SECTION("tuple")
    {
        tuple t1{1, 4.14, "text"s, "abc"};
        assert_deduction<tuple<int, double, string, const char*>>(t1);
    }

    SECTION("vector")
    {
        vector vec{1, 2, 3, 4};
        assert_deduction<vector<int>>(vec);        

        //vector vec2(vec.begin(), vec.end());
    }

    SECTION("array")
    {
        array arr{ 1, 2, 3, 4, 5 };
        assert_deduction<array<int, 5>>(arr);
    }

    SECTION("function")
    {
        function f = []() { return 42; };
        REQUIRE(f() == 42);
    }

    SECTION("smart_ptr")
    {
        //unique_ptr u(new int(42); // ERROR - no deduction for up

        shared_ptr<int> sp = make_shared<int>(42);
        weak_ptr wp = sp;
    }
}

template <typename T1, typename T2>
struct CData
{
    T1 value1;
    T2 value2;
};

template <typename T1, typename T2>
CData(T1, T2) -> CData<T1, T2>;

TEST_CASE("aggregates & deduction guide")
{
    CData cd1{1, 3.14};
}