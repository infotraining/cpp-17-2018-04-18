#include <iostream>
#include <string>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

namespace Cpp17Details
{
    template <typename T1, typename T2>
    constexpr auto is_same_v = is_same<T1, T2>::value;
}

template <typename T>
std::string as_string(T x)
{
    if constexpr (is_same_v<T, string>)
    {
        return x;
    }
    else if constexpr (is_arithmetic_v<T>)
    {
        return to_string(x);
    }
    else
        return string(x);
}

TEST_CASE("as_string with compile-time if")
{
    SECTION("strings")
    {
        string s = "hello";
        auto result = as_string(s);

        REQUIRE(result == "hello"s);
    }

    SECTION("numbers")
    {
        int x = 42;
        auto result = as_string(x);
    }

    SECTION("floating points")
    {
        double pi = 3.14;

        auto result = as_string(pi);

        REQUIRE(result == "3.140000");
    }

    SECTION("c-string")
    {
        auto result = as_string("text");

        REQUIRE(result == "text");
    }
}

template <typename T>
void process_array(const T& data)
{
    if constexpr (size(data) <= 255)
    {
        cout << "Processing small array" << endl;
    }
    else
        cout << "Processing large array" << endl;
}

TEST_CASE("processing arrays")
{
    int tab1[42] = {};
    process_array(tab1);

    int tab2[512] = {};
    process_array(tab2);

    array<int, 1024> arr = {{}};
    process_array(arr);
}

///////////////////////////////////////////
//  Head-Tail idiom

namespace BeforeCpp17
{
    void print()
    {
    }

    template <typename Head, typename... Tail>
    void print(Head&& head, Tail&&... tail)
    {
        cout << head << endl;
        print(std::forward<Tail>(tail)...);
    }
}

template <typename Head, typename... Tail>
void print(Head&& head, Tail&&... tail)
{
    cout << head << endl;

    if constexpr (sizeof...(tail) > 0)
        print(std::forward<Tail>(tail)...);
}

TEST_CASE("print all")
{
    print(1, 3.14, "text"s, "ctext");
}

///////////////////////////////////////////////////////////////////
// advance in std lib

/////////////////////////////////////////////////////
// constexpr if in standard library

template<typename Iterator, typename Distance>
void advance(Iterator& pos, Distance n) {
    using IterCategory = typename std::iterator_traits<Iterator>::iterator_category;

    if constexpr (std::is_same_v<IterCategory, std::random_access_iterator_tag>) {
        pos += n;
    }
    else if constexpr (std::is_same_v<IterCategory,
                       std::bidirectional_iterator_tag>) {
        if (n >= 0) {
            while (n--) {
                ++pos;
            }
        }
        else {
            while (n++) {
                --pos;
            }
        }
    }
    else { // input_iterator_tag
        while (n--) {
            ++pos;
        }
    }
}
