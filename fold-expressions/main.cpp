#include <iostream>
#include <string>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    auto sum()
    {
        return 0;
    }

    template <typename Head, typename... Tail>
    auto sum(Head head, Tail... tail)
    {
        return head + sum(tail...);
    }
}

template <typename... Args>
auto fold_sum(Args... args)
{
    return (... + args); // left-fold
}

template <typename... Args>
auto foldr_sum(Args... args)
{
    return (args + ...); // right fold
}

template <typename... Args>
bool all_true(Args... args)
{
    return (... && args);
}

template <typename... Args>
void print_all(Args... args)
{
    (cout << ... << args) << "\n"; // left binary fold
}

TEST_CASE("folds")
{
    REQUIRE(BeforeCpp17::sum(1, 2, 3, 4) == 10);

    REQUIRE(fold_sum(1, 2, 3, 4) == 10);
    REQUIRE(foldr_sum(1, 2, 3, 4, 5) == 15);

    REQUIRE(fold_sum("one"s, "two", "three") == "onetwothree");
    REQUIRE(foldr_sum("one", "two", "three"s) == "onetwothree");

    REQUIRE(all_true(true, 1, 1, true) == true);

    print_all(1, 2, "text");
    // (cout << 1 ) << 2) << "text") << endl;
 }

struct Window
{
    void show()
    {
        cout << "Showing window" << endl;
    }
};

struct Toolbar
{
    void show()
    {
        cout << "Showing toolbar" << endl;
    }
};

struct Button
{
    void show()
    {
        cout << "Showing button" << endl;
    }
};

TEST_CASE("calling show")
{
    Window wnd;
    Toolbar toolbar;
    Button button;

    auto printer = [](auto&&... args) {
        (args.show(), ...);
    };

    printer(wnd, Toolbar{}, button);
}