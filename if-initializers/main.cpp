#include <iostream>
#include <string>
#include <mutex>
#include <queue>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

TEST_CASE("if with initializers")
{
    vector<int> vec = { 5, 245, 635456, 665, 324, 3425, 11 };

    SECTION("Before C++17")
    {
        auto pos = find(begin(vec), end(vec), 665);

        if (pos != end(vec))
        {
            cout << "Item " << *pos << " has been found..." << endl;
        }
    }

    SECTION("Since C++17")
    {
        if (auto pos = find(begin(vec), end(vec), 665); pos != end(vec))
        {
            cout << "Item " << *pos << " has been found..." << endl;
        }
    }
}

TEST_CASE("use case with mutex")
{
    queue<int> q;
    mutex mtx_q;

    SECTION("Before C++17")
    {
        {
            lock_guard lk{mtx_q};
            if (!q.empty())
            {
                int value = q.front();
                q.pop();
            }
        }
        //...
    }

    if (lock_guard lk{mtx_q}; !q.empty())
    {
        int value = q.front();
        q.pop();
    }
}

enum class Status
{
    bad, on, off
};

struct Gadget
{
    Status status;
};

Gadget create_gadget()
{
    return Gadget{ Status::on };
}

TEST_CASE("switch with initializer")
{
    switch(auto g = create_gadget(); g.status)
    {
        case Status::on:
            cout << "Device is on" << endl;
            break;
        case Status::off:
            cout << "Device is off" << endl;
            break;
        default:
            cout << "Error occured" << endl;
    }
}

TEST_CASE("")
{
    if (auto [a, b, c] = tuple(1, 3.14, "text"s); true)
    {
        REQUIRE(a == 1);
        REQUIRE(c == "text"s);
    }
}