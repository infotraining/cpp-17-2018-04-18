#ifndef GADGET_HPP
#define GADGET_HPP

class Gadget
{
public:
    Gadget()
    {
        ++counter;
    }

    static inline int counter{};
    static constexpr auto id = "Gadget";
};

#endif