#include <iostream>
#include <string>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "gadget.hpp"

using namespace std;

TEST_CASE("static inline")
{
    Gadget g1{};
    Gadget g2{};

    REQUIRE(Gadget::counter == 2);
}