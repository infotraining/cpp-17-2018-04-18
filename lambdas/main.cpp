#include <iostream>
#include <string>
#include <functional>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

class Gadget
{
    string name_;

public:
    Gadget(string name) : name_{move(name)}
    {
        cout << "Gadget(" << name_ << " - " << this << ")\n";
    }

    Gadget(const Gadget& source) : name_{source.name_}
    {
        cout << "Gadget(cc" << name_ << " - " << this << ")\n";
    }

    ~Gadget()
    {
        cout << "~Gadget(" << name_ << " - " << this << ")\n";
    }

    void play()
    {
        cout << "Playing: " << name_ << endl;
    }

    auto report()
    {
        return [*this] { cout << "using Gadget(" << name_ << " - " << this << ")" << endl; };
    }
};

TEST_CASE("using capture by *this")
{
    function<void()> reporter;
    {
        Gadget g{"guitar"};

        reporter = g.report();
    }

    reporter();
}

TEST_CASE("constexpr lambda - implicit constexpr")
{
    auto square = [](size_t x) { return x * x; };

    array<int, square(8)> arr1 = {};
    int tab1[square(10)];

    static_assert(size(arr1) == 64);    

    SECTION("explicit contexpr")
    {
        constexpr array<int, square(16)> values = { 1, 2, 3, 4, 5, 6, 7 };

        auto sum = [](const auto& data) constexpr {
            long result{};
            for(auto it = begin(data); it != end(data); ++it)
                result += *it;
            return result;
        };

        static_assert(sum(values) == 28);
    }
}

template <typename Iter, typename Pred>
constexpr Iter my_find_if(Iter first, Iter last, Pred pred_f)
{
    for(Iter it = first; it != last; ++it)
        if (pred_f(*it))
            return it;
    return last;
}

TEST_CASE("constexpr find")
{
    constexpr array<int, 8> data = { 7, 235, 645, 234, 6545, 235 };
    constexpr auto size_of_array = *my_find_if(begin(data), end(data), [](int x) { return x % 5 == 0;});

    array<int, size_of_array> data2 = {};

    static_assert(size(data2) == 235);
}