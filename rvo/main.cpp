#include <iostream>
#include <string>
#include <atomic>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

struct NoCopyAndMove
{
    int x;

    NoCopyAndMove(int x) : x{x} {}
    NoCopyAndMove(const NoCopyAndMove&) = delete;
    NoCopyAndMove(NoCopyAndMove&&) = delete;
    NoCopyAndMove& operator=(const NoCopyAndMove&) = delete;
    NoCopyAndMove& operator=(NoCopyAndMove&&) = delete;
    ~NoCopyAndMove() = default;
};

NoCopyAndMove create_object_rvo()
{
    return NoCopyAndMove{42};
}

// NoCopyAndMove create_object_nrvo()
// {
//     NoCopyAndMove obj{665};
//     return obj;
// }

void consume(NoCopyAndMove arg)
{
    cout << arg.x << endl;
}

void consume_by_ref(const NoCopyAndMove& obj)
{
    cout << obj.x << endl;
}

TEST_CASE("RVO in C++17")
{
    NoCopyAndMove obj = create_object_rvo();

    REQUIRE(obj.x == 42);

    consume(create_object_rvo());
    consume(NoCopyAndMove{12});
    consume_by_ref(create_object_rvo());
}

template <typename T, typename... Args>
T create(Args&&... args)
{
    return T{std::forward<Args>(args)...};
}

TEST_CASE("")
{
    auto a = create<atomic<int>>(42);
}