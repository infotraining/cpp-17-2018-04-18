#include <iostream>
#include <string>
#include <string_view>
#include <optional>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

TEST_CASE("string-view")
{
    const char* ctext = "abc";
    string text = "abc";
    string_view text_sv = text;

    cout << text_sv << endl;
    REQUIRE(text_sv.length() == 3);

    auto filename = "data.txt"sv;
    filename.remove_suffix(4);

    REQUIRE(filename == "data");

    SECTION("default constructed")
    {
        string_view sv_empty;

        REQUIRE(sv_empty.data() == nullptr);
    }
}

optional<int> as_int(string_view value)
{
    try
    {
        int int_value = stoi(string(value));
        return int_value;
    }
    catch(...)
    {
        return std::nullopt;
    }
}

//optional<int> as_int(string_view value)
//{
//    int int_value;
//    auto [ptr, errc] = from_chars(value.data(), value.data() + value.size(), &int_value);

//    if (errc == std::errc{})
//    {
//        return int_value;
//    }

//    return nullopt;
//}

TEST_CASE("passing string_view as param")
{
    auto result = as_int("10"s);

    REQUIRE(*result == 10);

    result = as_int("x");

    REQUIRE_FALSE(result.has_value());
}


TEST_CASE("sv problems")
{
    string_view text = "abc"s;

    cout << text << endl;
}

string get_text()
{
    return "abc";
}

TEST_CASE("string_view considered harmful")
{
    SECTION("dangling sv")
    {
        string_view sv = get_text();
        // sv holds dangling pointer
    }

    SECTION("returning sv")
    {
        auto substring = [](const string& text, size_t idx = 0) -> string_view {
            return string_view(text.c_str(), idx);
        };

        string s = "abc";
        REQUIRE(substring(s, 2) == "ab"sv);

        auto ss = substring(get_text(), 2);
        // ss holds dangling pointer
    }
}
