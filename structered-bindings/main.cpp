#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    tuple<int, int, double> calc_stats(const vector<int>& data)
    {
        vector<int>::const_iterator min, max;
        tie(min, max) = minmax_element(begin(data), end(data));
        double avg    = accumulate(begin(data), end(data), 0.0) / data.size();

        return make_tuple(*min, *max, avg);
    }
}

namespace Cpp17
{
    tuple<int, int, double> calc_stats(const vector<int>& data)
    {
        auto[min_pos, max_pos] = minmax_element(begin(data), end(data));
        double avg             = accumulate(begin(data), end(data), 0.0) / data.size();

        return make_tuple(*min_pos, *max_pos, avg);
    }
}

TEST_CASE("structured binding")
{
    vector<int> data = {8, 12, 312, 454, 23, 1, 32, 432};

    SECTION("Cpp11")
    {
        auto result = BeforeCpp17::calc_stats(data);

        REQUIRE(get<0>(result) == 1);
        REQUIRE(get<1>(result) == 454);
        REQUIRE(get<2>(result) == Approx(159.25));
    }

    SECTION("allows unpack a tuple into variables")
    {
        auto[min, max, avg] = BeforeCpp17::calc_stats(data);

        REQUIRE(min == 1);
        REQUIRE(max == 454);
        REQUIRE(avg == Approx(159.25));
    }

    SECTION("allows unpack structure to variables")
    {
        struct Date
        {
            int y;
            string month;
            int d;
        };

        Date d1{2018, "Apr", 18};

        auto[year, month, day] = d1;

        REQUIRE(year == 2018);
        REQUIRE(month == "Apr");
        REQUIRE(day == 18);
    }

    SECTION("unpacking std::array")
    {
        auto get_data = [] {
            array<int, 2> coord = {{1, 2}};
            return coord;
        };       

        auto[x, y] = get_data();

        REQUIRE(x == 1);
        REQUIRE(y == 2);
    }

    SECTION("unpacking native array")
    {
        auto get_data = []() -> int(&)[3]
        {
            static int coords[3] = {1, 2, 3};
            return coords;
        };

        auto[x, y, z] = get_data();

        REQUIRE(x == 1);
        REQUIRE(y == 2);
        REQUIRE(z == 3);
    }
}

TEST_CASE("&, const, volatile, align modifiers")
{
    tuple<int, string, double> tpl{1, "text"s, 3.14};

    SECTION("references")
    {
        auto & [ first, second, third ] = tpl;

        second = "changed text"s;

        REQUIRE(get<1>(tpl) == "changed text"s);

        SECTION("ref to rvalue")
        {
            const auto & [ first, second, third ] = make_tuple(1, "text"s, 3.14);

            SECTION("auto&& alows to use move semantics")
            {
                auto && [ f, s ] = make_pair(1, "text"s);

                string text = move(s);

                REQUIRE(text == "text"s);
                REQUIRE(s == ""s);
            }
        }
    }

    SECTION("alignas")
    {
        alignas(16) auto[first, second, third] = tpl;
    }
}

TEST_CASE("how it works")
{
    struct Time
    {
        int h, m;
    };

    Time t1{10, 22};

    auto[hours, minutes] = t1;

    SECTION("is interpreted")
    {
        auto temp     = t1;
        auto& hours   = temp.h;
        auto& minutes = temp.m;
    }        
}

TEST_CASE("use cases")
{
    map<int, string> dictionary = {{1, "one"}, {5, "five"}, {3, "three"}};

    SECTION("iteration over map")
    {
        for (const auto& item : dictionary)
        {
            cout << item.first << " " << item.second << endl;
        }

        SECTION("since c++17")
        {
            for (const auto & [ key, value ] : dictionary)
            {
                cout << key << " " << value << endl;
            }
        }
    }

    SECTION("inserting into associative container")
    {
        if (auto[pos, is_success] = dictionary.insert(pair(2, "two"s)); is_success)
        {
            const auto & [ key, value ] = *pos;
            cout << "Item " << value << " has been inserted" << endl;
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////
// tuple protocol for structure bindings

enum Something
{
};

template <>
struct std::tuple_size<Something>
{
    static unsigned int const value = 2;
};

template <>
struct std::tuple_element<0, Something>
{
    using type = int;
};

template <>
struct std::tuple_element<1, Something>
{
    using type = double;
};

namespace TemplateSpecialization
{
    template <size_t>
    auto get(const Something&);

    template <>
    auto get<0>(const Something&)
    {
        return 42;
    }

    template <>
    auto get<1>(const Something&)
    {
        return 3.14;
    }
}

template <size_t N>
auto get(const Something&)
{
    if constexpr (N == 0)
        return 42;
    else if constexpr (N == 1)
        return 3.14;
}

TEST_CASE("structured binding with tuple protocol")
{
    auto[first, second] = Something();

    REQUIRE(first == 42);
    REQUIRE(second == Approx(3.14));
}
