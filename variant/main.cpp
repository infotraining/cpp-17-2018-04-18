#include <iostream>
#include <string>
#include <variant>

#define CATCH_CONFIG_MAIN

#include "catch.hpp"

using namespace std;

TEST_CASE("variant")
{
    variant<int, string, double> var1;

    REQUIRE(get<int>(var1) == 0);

    var1 = 3.14;
    var1 = "text"s;

    variant<int, string, double> var2{"text"s};

    REQUIRE(get<string>(var2) == "text"s);

    SECTION("implicit conversion can cause troubles")
    {
        variant<string, double, bool> var3 = "ctext";

        REQUIRE_THROWS_AS(get<string>(var3), bad_variant_access);

        REQUIRE(var3.index() == 2);
        REQUIRE(holds_alternative<bool>(var3));
        REQUIRE(get<bool>(var3) == true);
        REQUIRE(get<2>(var3) == true);

        string* ptr_value = get_if<string>(&var3);
        REQUIRE_FALSE(ptr_value);
    }

    SECTION("can hold the same type more than once")
    {
        variant<int, string, int> var = "text"s;

        var.emplace<0>(42);

        //REQUIRE(holds_alternative<int>(var)); // ERROR

        REQUIRE(get<0>(var) == 42);

        var.emplace<2>(665);

        REQUIRE(get<2>(var) == 665);
    }
}

struct Gadget
{
    int id;
    string name;

    Gadget(int id, string name)
        : id{id}
        , name{move(name)}
    {
    }
};

struct Person
{
    string name;

    Person(string name)
        : name{move(name)}
    {
    }
};

TEST_CASE("first item on list of types must by default constructible")
{
    //variant<Gadget, int, string> var1; // ERROR

    variant<int, Gadget, string> var2;

    variant<monostate, Gadget, Person> var3;

    REQUIRE(holds_alternative<monostate>(var3));
    REQUIRE(var3.index() == 0);

    var3 = Gadget{1, "mp3 player"};
    var3.emplace<Person>("Jan");

    REQUIRE(get<Person>(var3).name == "Jan"s);
}

struct Printer
{
    void operator()(int x) const
    {
        cout << "int: " << x << endl;
    }

    void operator()(double x) const
    {
        cout << "double: " << x << endl;
    }

    void operator()(const string& x) const
    {
        cout << "string: " << x << endl;
    }

    void operator()(const Gadget& g) const
    {
        cout << "Gadget(id: " << g.id << ", name: " << g.name << ")" << endl;
    }
};

struct Lambda_7236423784
{
    template <typename T>
    void operator()(const T& item) const
    {
        cout << item << endl;
    }
};

template <typename... Closure>
struct Overloader : Closure...
{
    using Closure::operator()...;
};

template <typename... Closure>
Overloader(Closure...)->Overloader<Closure...>;

TEST_CASE("visiting variants")
{
    variant<int, double, string, Gadget> var1{"text"s};

    Printer prn;
    visit(prn, var1);

    var1                      = 3.14;
    variant<int, Gadget> var2 = Gadget{1, "ipad"};

    visit([](const auto& item) {
        if constexpr (is_same_v<Gadget, decay_t<decltype(item)>>)
        {
            cout << "Gadget(id: " << item.id << ", name: " << item.name << ")" << endl;
        }
        else
            cout << item << endl;
    },
          var2);

    auto local_visitor = Overloader{
        [](const Gadget& item) {
            cout << "Gadget(id: " << item.id << ", name: " << item.name << ")" << endl;
        },
        [](const auto& x) {
            cout << x << endl;
        }};

    visit(local_visitor, var1);
}

struct ErrorCode
{
    string description;
};

variant<string, ErrorCode> load_file(const string& filename)
{
    if (filename.length() > 5)
        return ErrorCode{"file to long"};

    return "File content: "s + filename;
}

TEST_CASE("use case for variant")
{
    visit(Overloader{
              [](const string& content) {
                  cout << content << endl;
              }/*,
              [](ErrorCode ec) {
                  cout << ec.description << endl;
              }}*/,
          load_file("abcfsdfsdfsdf"));
}
